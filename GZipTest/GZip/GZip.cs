﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipTest.GZip
{
    class GZip : IArchive, IDisposable
    {
        private static int CountThreads = Environment.ProcessorCount;
        private static byte[][] Data = new byte[CountThreads][];//= new byte[10][];
        private static byte[][] DataCompress = new byte[CountThreads][];
        bool IsCancel=false;

        public int Compress(string CompressFileName, string ArchiveFileName)
        {
            int DefSizePart = (int)Math.Pow(2, 20);
            int _CustomSizePart;
            Thread[] ThrdsCmprs;
            try
            {
                using (FileStream CompressFile = new FileStream(Environment.CurrentDirectory + "//" + CompressFileName, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    using (FileStream ArchFile = new FileStream(Environment.CurrentDirectory + "//" + ArchiveFileName, FileMode.Create))
                    {
                        Console.WriteLine("Compressing");
                        while (CompressFile.Position < CompressFile.Length)
                        {
                            if (IsCancel)
                                break;
                            ThrdsCmprs = new Thread[CountThreads];
                            for (int PartsCounter = 0; (PartsCounter < CountThreads) && (CompressFile.Position < CompressFile.Length); PartsCounter++)
                            {
                                if (CompressFile.Length - CompressFile.Position <= DefSizePart)
                                {
                                    _CustomSizePart = (int)(CompressFile.Length - CompressFile.Position);
                                }
                                else
                                {
                                    _CustomSizePart = DefSizePart;
                                }
                                Data[PartsCounter] = new byte[_CustomSizePart];
                                CompressFile.Read(Data[PartsCounter], 0, _CustomSizePart);

                                ThrdsCmprs[PartsCounter] = new Thread(PartForСompression);
                                ThrdsCmprs[PartsCounter].Start(PartsCounter);
                            }

                            for (int PartsCounter = 0; (PartsCounter < CountThreads) && (ThrdsCmprs[PartsCounter] != null);)
                            {
                                if (ThrdsCmprs[PartsCounter].ThreadState == System.Threading.ThreadState.Stopped)
                                {
                                    var ttt = BitConverter.GetBytes(DataCompress[PartsCounter].Length);
                                    var tmpCompresData = new byte[DataCompress[PartsCounter].Length - 6];
                                    ttt.CopyTo(tmpCompresData, 0);
                                    DataCompress[PartsCounter].Skip(10).ToArray().CopyTo(tmpCompresData, 4);
                                    ArchFile.Write(tmpCompresData, 0, tmpCompresData.Length);
                                    PartsCounter++;
                                }
                            }
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 1;
            }
        }

        public static void PartForСompression(object i)
        {
            int Index = (int)i;
            using (MemoryStream ms = new MemoryStream(Index))
            {
                using (GZipStream gzs = new GZipStream(ms, CompressionMode.Compress))
                {
                    gzs.Write(Data[Index], 0, Data[Index].Length);
                }
                DataCompress[(int)i] = ms.ToArray();
            }
        }

        public int Decompress(string ArchiveFileName, string OutFileName)
        {
            int _CustomSizePart;
            int SizeCompressBlock;
            Thread[] ThrdsDcmp;

            try
            {
                using (FileStream ArchFile = new FileStream(Environment.CurrentDirectory + "//" + ArchiveFileName, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    using (FileStream DecompressFile = new FileStream(Environment.CurrentDirectory + "//" + OutFileName, FileMode.Append, FileAccess.Write))
                    {
                        Console.WriteLine("Decompressing");
                        byte[] LengthBuff = new byte[4];
                        while (ArchFile.Position < ArchFile.Length)
                        {
                            if (IsCancel)
                                break;
                            ThrdsDcmp = new Thread[CountThreads];
                            for (int PartsCounter = 0; (PartsCounter < CountThreads) && (ArchFile.Position < ArchFile.Length); PartsCounter++)
                            {
                                ArchFile.Read(LengthBuff, 0, 4);
                                SizeCompressBlock = BitConverter.ToInt32(LengthBuff, 0);
                                Data[PartsCounter] = new byte[SizeCompressBlock];
                                //ФОРМАТ БАЙТА в побитовой значимости
                                //                                +--------+
                                //                                |76543210|
                                //                                +--------+
                                // http://www.zlib.org/rfc-gzip.html#member-format  - спецификация
                                DataCompress[PartsCounter][0] = 31; //ID1
                                DataCompress[PartsCounter][1] = 139;//ID1
                                DataCompress[PartsCounter][2] = 8;  //CM = 8 обозначает метод «спящего» сжатия, который обычно используется gzip и который документирован в другом месте.
                                DataCompress[PartsCounter][3] = 0;  //FLG
                                LengthBuff.CopyTo(DataCompress[PartsCounter], 4); //MTIME - время в формате UNIX
                                DataCompress[PartsCounter][8] = 4;  //XFL 2 - максимальная компрессия 4 - быстрое сжатие
                                DataCompress[PartsCounter][9] = 0;  //OS - 0 -FAT filesystem (MS-DOS, OS/2, NT/Win32)  
                                                                    //     1 - Amiga
                                                                    //     2 - VMS(or OpenVMS)
                                                                    //     3 - Unix
                                                                    //     4 - VM / CMS
                                                                    //     5 - Atari TOS
                                                                    //     6 - HPFS filesystem(OS / 2, NT)
                                                                    //     7 - Macintosh
                                                                    //     8 - Z - System
                                                                    //     9 - CP / M
                                                                    //     10 - TOPS - 20
                                                                    //     11 - NTFS filesystem(NT)
                                                                    //     12 - QDOS
                                                                    //     13 - Acorn RISCOS
                                                                    //     255 - unknown
                                ArchFile.Read(DataCompress[PartsCounter], 10, SizeCompressBlock - 10);
                                _CustomSizePart = BitConverter.ToInt32(DataCompress[PartsCounter], SizeCompressBlock - 4);
                                Data[PartsCounter] = new byte[_CustomSizePart];
                                ThrdsDcmp[PartsCounter] = new Thread(PartForDecompress);
                                ThrdsDcmp[PartsCounter].Start(PartsCounter);
                            }
                            for (int PartsCounter = 0; (PartsCounter < CountThreads) && (ThrdsDcmp[PartsCounter] != null);)
                            {
                                if (ThrdsDcmp[PartsCounter].ThreadState == ThreadState.Stopped)
                                {
                                    DecompressFile.Write(Data[PartsCounter], 0, Data[PartsCounter].Length);
                                    PartsCounter++;
                                }
                            }
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR:" + ex.Message);
                return 1;
            }
        }

        public static void PartForDecompress(object i)
        {
            int Index = (int)i;
            using (MemoryStream ms = new MemoryStream(DataCompress[Index]))
            {
                using (GZipStream gzs = new GZipStream(ms, CompressionMode.Decompress))
                {
                    gzs.Read(Data[Index], 0, Data[Index].Length);
                }
            }
        }

        public void Dispose()
        {
            IsCancel = true;                
        }
    }
}
