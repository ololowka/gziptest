﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipTest.GZip
{
    interface IArchive
    {
        int Compress(string CompressFileName, string ArchiveFileName);

        int Decompress(string ArchiveFileName, string OutFileName);
    }
}
