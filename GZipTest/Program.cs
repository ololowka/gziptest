﻿using System;
using System.IO;

namespace GZipTest
{
    class Program
    {
        static GZip.GZip gz;
        static string[] _args;
        static void Main(string[] args)
        {
            if (args != null && args.Length > 1)
            {
                _args = args;
                Console.CancelKeyPress += new ConsoleCancelEventHandler(OverCtrlC);
                if (!string.IsNullOrWhiteSpace(args[0]))
                {
                    switch (args[0].ToLower())
                    {
                        case "compress":
                            {
                                if (!string.IsNullOrWhiteSpace(args[1]) && !string.IsNullOrWhiteSpace(args[2]))
                                {
                                    gz = new GZip.GZip();
                                    Console.WriteLine(gz.Compress(args[1].Trim(), args[2].Trim()));
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input or output file name");
                                    Console.WriteLine(1);
                                }
                            }
                            break;
                        case "decompress":
                            {
                                if (!string.IsNullOrWhiteSpace(args[1]) && !string.IsNullOrWhiteSpace(args[2]))
                                {
                                    gz = new GZip.GZip();
                                    Console.WriteLine(gz.Decompress(args[1].Trim(), args[2].Trim()));
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input or output file name");
                                    Console.WriteLine(1);
                                }

                            }
                            break;
                        default:
                            Console.WriteLine("Command not recognized");
                            Console.WriteLine(1);
                            break;
                    }
                }
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Command not recognized");
                Console.WriteLine(1);
                Console.ReadLine();
            }
        }

        private static void OverCtrlC(object sender, ConsoleCancelEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_args[0]))
                {
                    gz.Dispose();
                }
                else
                {
                    Console.WriteLine("No operation mode specified");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
